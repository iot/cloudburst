package server

import (
	"crypto/tls"
	"encoding/hex"
	"fmt"
	"net/http"

	"github.com/go-ocf/go-coap"
	"github.com/pion/dtls/v2"
	log "github.com/sirupsen/logrus"
)

// Listener represents the configuration for an HTTP/CoAP listener.
type Listener struct {
	Type         string `yaml:"type"`
	Address      string `yaml:"address"`
	CertFile     string `yaml:"tls_cert"`
	KeyFile      string `yaml:"tls_key"`
	PreSharedKey string `yaml:"psk"`
}

// ListAndServe starts a Listener for a Server.
func (l *Listener) ListAndServe(s *Server) error {
	switch l.Type {
	case "coap":
		return coap.ListenAndServe("udp", l.Address, s.coapMux, logErrorFunc)
	case "coaps":
		config, err := dtlsConfig(l.PreSharedKey, l.CertFile, l.KeyFile)
		if err != nil {
			return err
		}
		return coap.ListenAndServeDTLS("udp-dtls", l.Address, config, s.coapMux, logErrorFunc)
	case "coap+tcp":
		return coap.ListenAndServe("tcp", l.Address, s.coapMux, logErrorFunc)
	case "coaps+tcp":
		config, err := tlsConfig(l.CertFile, l.KeyFile)
		if err != nil {
			return err
		}
		return coap.ListenAndServeTLS("tcp-tls", l.Address, config, s.coapMux, logErrorFunc)
	case "http":
		return http.ListenAndServe(l.Address, s.httpMux)
	case "https":
		return http.ListenAndServeTLS(l.Address, l.CertFile, l.KeyFile, s.httpMux)
	default:
		return fmt.Errorf("unknown listener type %q", l.Type)
	}
}

// tlsConfig returns TLS configuration for certificate files.
func tlsConfig(certFile, keyFile string) (*tls.Config, error) {
	certs, err := tls.LoadX509KeyPair(certFile, keyFile)
	return &tls.Config{Certificates: []tls.Certificate{certs}}, err
}

// dtlsConfig returns DTLS configuration for a PSK or certificate files.
func dtlsConfig(psk, certFile, keyFile string) (*dtls.Config, error) {
	config := &dtls.Config{}
	if psk != "" {
		bytes, err := hex.DecodeString(psk)
		if err != nil {
			return nil, fmt.Errorf("invalid psk: %w", err)
		}
		config.PSK = func(hint []byte) ([]byte, error) {
			return bytes, nil
		}
		config.CipherSuites = []dtls.CipherSuiteID{dtls.TLS_PSK_WITH_AES_128_CCM_8}
	}

	if certFile != "" || keyFile != "" {
		certs, err := tls.LoadX509KeyPair(certFile, keyFile)
		if err != nil {
			return nil, err
		}
		config.Certificates = []tls.Certificate{certs}
	}
	return config, nil
}

// logErrorFunc logs any error given to the request.
func logErrorFunc(err error) bool {
	log.Warnf("CoAP error: %s", err)
	return true
}
