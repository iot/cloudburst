package server

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"

	"github.com/go-ocf/go-coap/codes"
	log "github.com/sirupsen/logrus"
	"github.com/ugorji/go/codec"
)

// errorNotFound contains the default 'not found' error.
var errorNotFound = NewErrorf(codes.NotFound, "not found")

// errorMessage represents the message returned to the client.
type errorMessage struct {
	XMLName xml.Name `json:"-" xml:"error" codec:"-"`
	Error   string   `json:"error" xml:",innerxml" codec:"error"`
}

// Error is a generic request/response error.
type Error struct {
	// The error causing this error.
	Err error

	// The corresponding response code.
	Code codes.Code
}

// NewError returns a new Error.
func NewError(code codes.Code, err error) *Error {
	return &Error{Err: err, Code: code}
}

// NewErrorf returns a new error with a custom description.
func NewErrorf(code codes.Code, format string, a ...interface{}) *Error {
	return &Error{Err: fmt.Errorf(format, a...), Code: code}
}

// Error returns the error text.
func (e *Error) Error() string {
	return fmt.Sprintf("%s (%v)", e.Err, e.Code)
}

// message returns the errorMessage for this Error.
func (e *Error) message() *errorMessage {
	return &errorMessage{Error: e.Err.Error()}
}

// Write writes the for a Request to a ResponseWriter.
func (e *Error) Write(w ResponseWriter, req *Request) {
	w.SetCode(e.Code)
	if !w.WriteErrors() {
		w.Write(nil)
		return
	}

	accept, err := findAccept(req.Accept)
	switch accept {
	case TypeCBOR:
		w.SetContentType(TypeCBOR)
		_, err = writeCbor(w, e.message())
	case TypeJSON, TypeApplication, TypeAny:
		w.SetContentType(TypeJSON)
		err = json.NewEncoder(w).Encode(e.message())
	case TypeXML:
		w.SetContentType(TypeXML)
		err = xml.NewEncoder(w).Encode(e.message())
	default:
		_, err = w.Write([]byte("Error: " + e.Err.Error()))
	}

	if err != nil {
		log.Errorf("Error writing error: %s", err)
	}
}

// writeCbor serializes an interface to an io.Writer.
func writeCbor(w io.Writer, i interface{}) (int, error) {
	var cbor codec.CborHandle
	var b []byte
	err := codec.NewEncoderBytes(&b, &cbor).Encode(i)
	if err != nil {
		return 0, err
	}

	return w.Write(b)
}
