package server

import (
	"fmt"
	"github.com/go-ocf/go-coap"
	"net"
	"net/url"
)

// mimeTypes contains a mapping of the MIME type strings to CoAP media types.
var mimeTypes = map[string]coap.MediaType{
	TypeCBOR:      coap.AppCBOR,
	TypeJSON:      coap.AppJSON,
	TypeXML:       coap.AppXML,
	TypeSenMLCBOR: coap.AppSenmlCbor,
	TypeSenMLJSON: coap.AppSenmlJSON,
	TypeSenMLXML:  310,
}

// CoAPResponseWriter is a CoAP based ResponseWriter.
type CoAPResponseWriter struct {
	coap.ResponseWriter
}

// NewCoAPResponseWriter creates a CoAP based ResponseWriter.
func NewCoAPResponseWriter(w coap.ResponseWriter) ResponseWriter {
	return &CoAPResponseWriter{w}
}

// SetContentType sets the content type using a given MIME type.
func (r *CoAPResponseWriter) SetContentType(t string) {
	f, ok := mimeTypes[t]
	if !ok {
		panic(fmt.Sprintf("invalid content type %q", t))
	}

	r.SetContentFormat(f)
}

// WriteErrors returns true if errors should be written to the client.
func (r *CoAPResponseWriter) WriteErrors() bool {
	return false
}

// NewCoAPRequest creates Request based on a coap.Request.
func NewCoAPRequest(r *coap.Request) *Request {
	query, _ := url.ParseQuery(r.Msg.QueryString())
	request := &Request{
		Method:     r.Msg.Code().String(),
		RemoteAddr: coapRemoteAddr(r),
		Path:       r.Msg.Path(),
		Query:      query,
		Body:       r.Msg.Payload(),
	}

	format, ok := r.Msg.Option(coap.ContentFormat).(coap.MediaType)
	if ok {
		request.ContentType = format.String()
	}

	accept, ok := r.Msg.Option(coap.Accept).(coap.MediaType)
	if ok {
		request.Accept = accept.String()
	}

	return request
}

// coapRemoteAddr returns the remote address for a coap.Request.
func coapRemoteAddr(req *coap.Request) string {
	switch a := req.Client.RemoteAddr().(type) {
	case *net.IPAddr:
		return a.IP.String()
	case *net.UDPAddr:
		return a.IP.String()
	case *net.TCPAddr:
		return a.IP.String()
	default:
		return a.String()
	}
}
