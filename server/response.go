package server

import (
	"io"

	"github.com/go-ocf/go-coap/codes"
)

// ResponseWriter is a generic response writer.
type ResponseWriter interface {
	io.Writer

	// SetCode sets the response code.
	SetCode(codes.Code)

	// SetContentType sets the content type using a given MIME type.
	SetContentType(t string)

	// WriteErrors returns true if errors should be written to the client.
	WriteErrors() bool
}
