package server

import (
	"git.snt.utwente.nl/dingen/cloudburst/influxdb"
	"github.com/go-ocf/go-coap"
	"github.com/go-ocf/go-coap/codes"
	"github.com/silkeh/senml"
	log "github.com/sirupsen/logrus"
	"net/http"
)

// Server represents a Cloudburst server.
type Server struct {
	influx    *influxdb.Client
	listeners []*Listener
	coapMux   *coap.ServeMux
	httpMux   *http.ServeMux
}

// NewServer creates a server backed by a certain InfluxDB client and set of listeners.
func NewServer(influxClient *influxdb.Client) *Server {
	server := &Server{
		influx:  influxClient,
		coapMux: coap.NewServeMux(),
		httpMux: http.NewServeMux(),
	}
	server.coapMux.DefaultHandleFunc(server.CoAPHandler)
	server.httpMux.HandleFunc("/", server.HTTPHandler)

	return server
}

// ListenAndServe starts handling requests based on the given listeners.
func (s *Server) ListenAndServe(listeners ...*Listener) error {
	ch := make(chan error)

	for _, listener := range listeners {
		l := listener
		log.Infof("Starting %q listener on %s", l.Type, l.Address)
		go func() {
			ch <- l.ListAndServe(s)
		}()
	}

	return <-ch
}

// HTTPHandler handles HTTP requests.
func (s *Server) HTTPHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	s.Handler(NewHTTPResponseWriter(w), NewHTTPRequest(r))
}

// CoAPHandler handles CoAP requests.
func (s *Server) CoAPHandler(w coap.ResponseWriter, r *coap.Request) {
	s.Handler(NewCoAPResponseWriter(w), NewCoAPRequest(r))
	if r.Msg.IsConfirmable() {
		w.Write(nil)
	}
}

// Handler handles generic API requests.
func (s *Server) Handler(w ResponseWriter, req *Request) {
	if len(req.Path) == 0 || len(req.Path) > 4 || req.Path[0] != "api" || req.Path[1] != "v1" {
		log.Infof("%s - %s %v - not found", req.RemoteAddr, req.Method, req.Path)
		errorNotFound.Write(w, req)
		return
	}

	log.Infof("%s - %s %v", req.RemoteAddr, req.Method, req.Path)
	err := s.apiHandler(w, req)
	if err != nil {
		log.Warnf("%s - error: %s (%s)", req.RemoteAddr, err.Err, err.Code)
		err.Write(w, req)
	}
}

// apiHandler handles API requests.
func (s *Server) apiHandler(w ResponseWriter, req *Request) *Error {
	switch req.Method {
	case http.MethodGet:
		return s.getHandler(w, req)
	case http.MethodPost:
		return s.postHandler(w, req)
	default:
		return NewErrorf(codes.MethodNotAllowed, "invalid method %q", req.Method)
	}
}

// getHandler handles API GET requests.
func (s *Server) getHandler(w ResponseWriter, req *Request) *Error {
	name, err := req.Measurement()
	if err != nil {
		return NewError(codes.NotFound, err)
	}

	host := req.PathHost()
	last := req.QueryLast()
	from, to, err := req.QueryTime()
	if err != nil {
		return NewError(codes.BadRequest, err)
	}

	accept, err := findAccept(req.Accept)
	if err != nil {
		return NewError(codes.UnsupportedMediaType, err)
	}

	measurements, err := s.influx.SenMLQuery(name, host, from, to, last)
	if err != nil {
		return NewError(codes.InternalServerError, err)
	}

	var data []byte
	switch accept {
	case TypeApplication, TypeAny:
		accept = TypeJSON
		fallthrough
	case TypeJSON, TypeSenMLJSON:
		data, err = senml.EncodeJSON(measurements)
	case TypeCBOR, TypeSenMLCBOR:
		data, err = senml.EncodeCBOR(measurements)
	case TypeXML, TypeSenMLXML:
		data, err = senml.EncodeXML(measurements)
	default:
		return NewErrorf(codes.BadRequest, "invalid content type %q", req.Accept)
	}

	if err != nil {
		return NewError(codes.InternalServerError, err)
	}

	w.SetContentType(accept)
	w.SetCode(codes.Content)
	w.Write(data)
	return nil
}

// postHandler handles API POST requests.
func (s *Server) postHandler(w ResponseWriter, req *Request) *Error {
	name, err := req.Measurement()
	if err != nil {
		return NewErrorf(codes.NotFound, "invalid measurement: %s", err)
	}

	data, err := req.SenML()
	if err != nil {
		return NewErrorf(codes.BadRequest, "invalid SenML: %s", err)
	}

	host, err := req.RequestHost()
	if err != nil {
		log.Warnf("Could not resolve host %s: %s", host, err)
	}

	err = s.influx.WriteSenML(name, host, "", data...)
	if err != nil {
		return NewError(codes.InternalServerError, err)
	}

	w.SetCode(codes.Created)
	return nil
}
