# CloudBurst
[![godoc](https://godoc.org/git.snt.utwente.nl/dingen/cloudburst?status.svg)](https://godoc.org/git.snt.utwente.nl/dingen/cloudburst)
[![pipeline status](https://git.snt.utwente.nl/dingen/cloudburst/badges/master/pipeline.svg)](https://git.snt.utwente.nl/dingen/cloudburst/commits/master)
[![coverage report](https://git.snt.utwente.nl/dingen/cloudburst/badges/master/coverage.svg)](https://git.snt.utwente.nl/dingen/cloudburst/commits/master)
[![goreportcard](https://goreportcard.com/badge/git.snt.utwente.nl/dingen/cloudburst)](https://goreportcard.com/report/git.snt.utwente.nl/dingen/cloudburst)

CloudBurst is an HTTP/CoAP frontend for storing/retrieving [SenML][] in/from InfluxDB using a simple API.

The API (documented below) works over HTTP(s) and CoAP(s) with CBOR (`application/senml+cbor`),
JSON (`application/senml+json`), and XML (`application/senml+xml`).
[SenSML][] or [SenML FETCH/PATCH][] are currently not supported.

## Installation

Download and build the application using:

```sh
go get "git.snt.utwente.nl/dingen/cloudburst/cmd/cloudburst"
```

The server can be started using `cloudburst -c <config file>`.
Example configuration can be found in `config.dist.yaml`.

## API

The API exposes endpoints based on [InfluxDB measurements][].
This allows one to store data from different sources separately.

### GET `/api/v1[/measurement][/host]`

Retrieve SenML data for `host` from `measurement`.

Optional path parameters:

| Parameter     | Description |
|---------------|-------------|
| `measurement` | Name of the InfluxDB measurement. `cloudburst` by default.
| `host`        | Hostname of the device. This may be a regular expression.

Optional query parameters:

| Parameter | Description |
|-----------|-------------|
| `last`    | Only retrieve the last values for every host.
| `from`    | Unix timestamp in seconds, or duration (in the past). Defaults to `1h`.
| `to`      | Unix timestamp in seconds, or duration (in the past). Defaults to `0s`.

Optional headers:

| Parameter | Description |
|-----------|-------------|
| `Accept`  | SenML MIME type. JSON by default.

### POST `/api/v1[/measurement][/host]`

Store SenML data for `host` in `measurement`.

Optional path parameters:

| Parameter     | Description |
|---------------|-------------|
| `measurement` | Name of the InfluxDB measurement. `cloudburst` by default.
| `host`        | Hostname of the device.

Required headers:

| Parameter      | Description |
|----------------|-------------|
| `Content-Type` | SenML MIME type.

[SenML]: https://www.rfc-editor.org/rfc/rfc8428
[SensML]: https://www.rfc-editor.org/rfc/rfc8428#section-4.8
[SenML FETCH/PATCH]: https://www.rfc-editor.org/rfc/rfc8790
[InfluxDB measurements]: https://docs.influxdata.com/influxdb/v2.0/reference/key-concepts/data-elements/#measurement
