package easyenergy

import (
	"time"
)

const (
	// URL is the URL to the Easy Energy XML API
	URL = "https://mijn.easyenergy.com/nl/api/tariff/getapxtariffs"
)

// EnergyTariff represents an hourly energy price from a supplier
type EnergyTariff struct {
	Time         time.Time `json:"Timestamp"`
	SupplierID   int       `json:"SupplierId"`
	TariffUsage  float64   `json:"TariffUsage"`
	TariffReturn float64   `json:"TariffReturn"`
}
