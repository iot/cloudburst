package easyenergy

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/silkeh/senml"
)

// Client is a HTTP client for the EasyEnergy API
type Client struct {
	*http.Client
}

// NewClient creates a new HTTP client for the EasyEnergy API
func NewClient(cli *http.Client) *Client {
	if cli == nil {
		cli = http.DefaultClient
	}
	return &Client{Client: cli}
}

// Get retrieves the latest data from the EasyEnergy API
func (c *Client) Get(start, end time.Time) (data []EnergyTariff, err error) {
	data = make([]EnergyTariff, 0)
	apiURL := fmt.Sprintf("%s?startTimestamp=%s&endTimestamp=%s",
		URL, encodeTime(start), encodeTime(end))

	var resp *http.Response
	resp, err = c.Client.Get(apiURL)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		var e Error
		err = json.NewDecoder(resp.Body).Decode(&e)
		if err != nil {
			return
		}
		return nil, e.Error()
	}

	err = json.NewDecoder(resp.Body).Decode(&data)
	return
}

// GetSenML collects measurements from EasyEnergy and converts them to SenML
func (c *Client) GetSenML(start, end time.Time) ([]senml.Measurement, error) {
	data, err := c.Get(start, end)
	if err != nil {
		return nil, fmt.Errorf("error collecting EasyEnergy data: %w", err)
	}

	measurements := make([]senml.Measurement, 0, len(data)*2)
	for _, tariff := range data {
		prefix := strconv.Itoa(tariff.SupplierID) + ":"
		measurements = append(measurements,
			senml.NewValue(prefix+"tariff_usage", tariff.TariffUsage, senml.None, tariff.Time, 0),
			senml.NewValue(prefix+"tariff_return", tariff.TariffReturn, senml.None, tariff.Time, 0),
		)
	}

	return measurements, nil
}

// encodeTime URL-encodes a time for the EasyEnergy API.
func encodeTime(t time.Time) string {
	return url.QueryEscape(t.UTC().Format(time.RFC3339))
}
