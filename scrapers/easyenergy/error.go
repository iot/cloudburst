package easyenergy

import "errors"

// Error represents an error message from the API
type Error struct {
	Message string
}

// Error returns the error for this Error.
func (e *Error) Error() error {
	return errors.New(e.Message)
}
