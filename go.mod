module git.snt.utwente.nl/dingen/cloudburst

go 1.14

require (
	github.com/go-ocf/go-coap v0.0.0-20200511140640-db6048acfdd3
	github.com/influxdata/influxdb1-client v0.0.0-20200827194710-b269163b24ab
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/magefile/mage v1.11.0 // indirect
	github.com/pion/dtls/v2 v2.0.8
	github.com/silkeh/senml v0.0.0-20210225180226-d67cf039d26c
	github.com/sirupsen/logrus v1.8.0
	github.com/ugorji/go/codec v1.2.4
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	golang.org/x/sys v0.0.0-20210228012217-479acdf4ea46 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
