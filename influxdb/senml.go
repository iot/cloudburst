package influxdb

import (
	"encoding/base64"
	"encoding/json"
	"github.com/influxdata/influxdb1-client/models"
	influxdb "github.com/influxdata/influxdb1-client/v2"
	"github.com/silkeh/senml"
	"time"
)

// InfluxDB tag and field values.
const (
	BooleanField    = "bool"
	DataField       = "data"
	StringField     = "string"
	SumField        = "sum"
	ValueField      = "value"
	UpdateTimeField = "update_time"
	HostTag         = "host"
	NameTag         = "name"
	UnitTag         = "unit"
	TimeField       = "time"
)

// PointsFromSenML creates a list of InfluxDB points from a list of SenML measurements.
func PointsFromSenML(name, host string, list []senml.Measurement) (pts []*influxdb.Point, err error) {
	pts = make([]*influxdb.Point, len(list))

	for i, measurement := range list {
		pts[i], err = NewPoint(name, host, measurement)
		if err != nil {
			return
		}
	}

	return
}

// NewPoint returns an InfluxDB point for a SenML measurement.
func NewPoint(name, host string, measurement senml.Measurement) (*influxdb.Point, error) {
	tags := map[string]string{
		HostTag: host,
		NameTag: measurement.Attrs().Name,
		UnitTag: string(measurement.Attrs().Unit),
	}

	var field string
	var value interface{}
	switch m := measurement.(type) {
	case *senml.Boolean:
		field = BooleanField
		value = m.Value
	case *senml.Data:
		field = DataField
		value = base64.StdEncoding.EncodeToString(m.Value)
	case *senml.Sum:
		field = SumField
		value = m.Value
	case *senml.String:
		field = StringField
		value = m.Value
	case *senml.Value:
		field = ValueField
		value = m.Value
	}

	fields := map[string]interface{}{
		field: value, UpdateTimeField: measurement.Attrs().UpdateTime}

	return influxdb.NewPoint(name, tags, fields, measurement.Attrs().Time)
}

// NewSenML creates a new JSONPoint from an InfluxDB row
func NewSenML(row *models.Row) (m []senml.Measurement, err error) {
	var commonAttributes senml.Attributes

	// Copy tags
	for t, v := range row.Tags {
		switch t {
		case HostTag:
			commonAttributes.Name = v + ":" + commonAttributes.Name
		case NameTag:
			commonAttributes.Name += v
		case UnitTag:
			commonAttributes.Unit = senml.Unit(v)
		}
	}

	// Parse values
	m = make([]senml.Measurement, len(row.Values))
	for j, value := range row.Values {
		attributes := senml.Attributes{
			Name: commonAttributes.Name,
			Unit: commonAttributes.Unit,
		}

		for i, v := range value {
			if v == nil {
				continue
			}

			switch row.Columns[i] {
			case HostTag:
				attributes.Name = v.(string) + ":" + attributes.Name
			case NameTag:
				attributes.Name += v.(string)
			case UnitTag:
				attributes.Unit = senml.Unit(v.(string))
			case TimeField:
				t, _ := v.(json.Number).Int64()
				attributes.Time = time.Unix(0, t)
			case UpdateTimeField:
				attributes.UpdateTime, _ = time.ParseDuration(v.(string))
			case BooleanField:
				m[j] = &senml.Boolean{Attributes: attributes, Value: v.(bool)}
			case DataField:
				data, _ := base64.StdEncoding.DecodeString(v.(string))
				m[j] = &senml.Data{Attributes: attributes, Value: data}
			case SumField:
				f, _ := v.(json.Number).Float64()
				m[j] = &senml.Sum{Attributes: attributes, Value: f}
			case StringField:
				m[j] = &senml.String{Attributes: attributes, Value: v.(string)}
			case ValueField:
				f, _ := v.(json.Number).Float64()
				m[j] = &senml.Value{Attributes: attributes, Value: f}
			}
		}
	}
	return
}
