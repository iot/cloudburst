package main

import (
	"flag"
	"git.snt.utwente.nl/dingen/cloudburst/influxdb"
	"git.snt.utwente.nl/dingen/cloudburst/server"

	log "github.com/sirupsen/logrus"
)

func main() {
	var configFile string
	flag.StringVar(&configFile, "c", "config.yaml", "Configuration file")
	flag.Parse()

	// Load configuration
	log.Infof("Loading configuration from %s", configFile)
	config, err := loadConfig(configFile)
	if err != nil {
		log.Fatalf("Error loading configuration: %s", err)
	}

	// Set loglevel
	logLevel, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.Fatalf("Invalid log level: %s", err)
	}
	log.SetLevel(logLevel)

	// Create the InfluxDB client
	log.Debugf("Creating InfluxDB client, host: %s, db: %s, user: %s",
		config.InfluxDB.Host, config.InfluxDB.Database, config.InfluxDB.Username)
	influxClient, err := influxdb.NewClient(&config.InfluxDB)
	if err != nil {
		log.Fatalf("Error connecting to InfluxDB: %s", err)
	}
	defer influxClient.Close()

	// Start the server
	srv := server.NewServer(influxClient)
	log.Fatal(srv.ListenAndServe(config.Listeners...))
}
