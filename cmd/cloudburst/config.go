package main

import (
	"git.snt.utwente.nl/dingen/cloudburst/server"
	"io/ioutil"

	"git.snt.utwente.nl/dingen/cloudburst/influxdb"
	"gopkg.in/yaml.v2"
)

// Config is used for the main configuration
type Config struct {
	LogLevel  string                `yaml:"log_level"`
	InfluxDB  influxdb.ClientConfig `yaml:"influxdb"`
	Listeners []*server.Listener    `yaml:"listeners"`
}

// loadConfig loads configuration
func loadConfig(path string) (config *Config, err error) {
	config = new(Config)

	// Get configuration data
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return config, err
	}

	// Parse configuration
	err = yaml.Unmarshal(data, config)
	if err != nil {
		return
	}

	// Set default log level if empty
	if config.LogLevel == "" {
		config.LogLevel = "info"
	}

	// Set default listeners if empty
	if len(config.Listeners) == 0 {
		config.Listeners = []*server.Listener{
			{Type: "http", Address: ":8080"},
			{Type: "coap", Address: ":5683"},
			{Type: "coap+tcp", Address: ":5683"},
		}
	}

	return
}
