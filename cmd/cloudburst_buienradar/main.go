package main

import (
	"bytes"
	"flag"
	"log"
	"strings"
	"time"

	"git.snt.utwente.nl/dingen/cloudburst/scrapers/buienradar"
	"github.com/go-ocf/go-coap"
	"github.com/go-ocf/go-coap/codes"
	"github.com/silkeh/senml"
)

func request(client *buienradar.Client, server, measurement string, stations ...string) {
	m, err := client.GetSenML(stations...)
	if err != nil {
		log.Printf("Error collecting Buienradar data: %s", err)
		return
	}

	b, err := senml.EncodeCBOR(m)
	if err != nil {
		log.Printf("Error serializing CBOR: %s", err)
		return
	}

	conn, err := new(coap.Client).Dial(server)
	if err != nil {
		log.Printf("Error creating CoAP client: %s", err)
		return
	}
	defer conn.Close()

	msg, err := conn.Post("api/v1/"+measurement, coap.AppCBOR, bytes.NewBuffer(b))
	if err != nil {
		log.Printf("Error sending CoAP: %s", err)
		return
	}

	if msg.Code() != codes.Created {
		log.Printf("Error response %v: %q", msg.Code(), msg.Payload())
	}
}

func main() {
	var stations, server, measurement string
	var interval time.Duration

	flag.StringVar(&server, "server", "localhost:5683", "CoAP server")
	flag.StringVar(&measurement, "measurement", "buienradar", "Measurement name")
	flag.StringVar(&stations, "stations", "Twente", "Comma separated list of weather stations to collect")
	flag.DurationVar(&interval, "interval", 0, "Collection interval, 0 to scape only once.")
	flag.Parse()

	stationList := strings.Split(stations, ",")
	client := buienradar.NewClient(nil)
	request(client, server, measurement, stationList...)
	if interval == 0 {
		return
	}

	ticker := time.NewTicker(interval)
	for range ticker.C {
		request(client, server, measurement, stationList...)
	}
}
