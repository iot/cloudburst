package main

import (
	"bytes"
	"flag"
	"log"
	"time"

	"git.snt.utwente.nl/dingen/cloudburst/scrapers/easyenergy"
	"github.com/go-ocf/go-coap"
	"github.com/silkeh/senml"
)

func request(client *easyenergy.Client, server, measurement string, backfill, prefetch time.Duration) {
	m, err := client.GetSenML(time.Now().Add(-backfill), time.Now().Add(prefetch))
	if err != nil {
		log.Printf("Error collecting EasyEnergy data: %s", err)
		return
	}

	b, err := senml.EncodeCBOR(m)
	if err != nil {
		log.Printf("Error serializing CBOR: %s", err)
		return
	}

	conn, err := new(coap.Client).Dial(server)
	if err != nil {
		log.Printf("Error creating CoAP client: %s", err)
		return
	}
	defer conn.Close()

	_, err = conn.Post("api/v1/"+measurement, coap.AppCBOR, bytes.NewBuffer(b))
	if err != nil {
		log.Printf("Error sending CoAP: %s", err)
	}
}

func main() {
	var server, measurement string
	var interval, backfill, prefetch time.Duration

	flag.StringVar(&server, "server", "localhost:5683", "CoAP server")
	flag.StringVar(&measurement, "measurement", "easy_energy", "Measurement name")
	flag.DurationVar(&interval, "interval", 0, "Collection interval, 0 to scape only once")
	flag.DurationVar(&backfill, "backfill", 6*time.Hour, "Amount of time to backfill")
	flag.DurationVar(&prefetch, "prefetch", 6*time.Hour, "Amount of time to prefetch")
	flag.Parse()

	client := easyenergy.NewClient(nil)
	request(client, server, measurement, backfill, prefetch)
	if interval == 0 {
		return
	}

	ticker := time.NewTicker(interval)
	for range ticker.C {
		request(client, server, measurement, backfill, prefetch)
	}
}
